﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PathFinder : MonoBehaviour
{
    public MapReader mapReader;

    public GameObject wayPointPref;

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    public void VisualizePath()
    {
        if (mapReader != null) {
            List<OsmNode> path = PathFindingGraph.instance.CalculatePath(
                mapReader.nodes[mapReader.ways[5].NodeIDs[0]], 
                mapReader.nodes[mapReader.ways[3].NodeIDs[0]]);
            foreach (OsmNode node in path)
            {
                //Debug.Log(node.X + "  " + node.Y);
                Instantiate(wayPointPref, (Vector3)node - mapReader.bounds.Centre, Quaternion.identity);
            }
        }
    }
}
