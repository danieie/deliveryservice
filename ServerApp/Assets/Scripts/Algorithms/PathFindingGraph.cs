﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

class PathFindingGraph : MonoBehaviour
{
    public static PathFindingGraph instance;

    void Start()
    {
        instance = this;
    }

    public List<OsmNode> CalculatePath(OsmNode startNode, OsmNode endNode)
    {
        List<OsmNode> path = new List<OsmNode>();
        for (int i = 0; i < 20 ; i++)
        {
            if (startNode.neighbors.Count > 0)
            {
                for (int j = 0; j < startNode.neighbors.Count; j++)
                {
                    if (!path.Contains(startNode.neighbors[j]))
                    {
                        path.Add(startNode.neighbors[j]);
                        startNode = startNode.neighbors[j];
                        break;
                    }
                }
                
            }
        }
        return path;
    }
}
